from django.apps import AppConfig


class KalkulatorsederhanaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kalkulatorSederhana'
