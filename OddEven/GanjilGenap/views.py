from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, "input.html")


def ganjil_genap(request):

    num1 = request.POST['num1']
    num2 = request.POST['num2']

    if num1.isdigit() and num2.isdigit():
        a = int(num1)
        b = int(num2)
        hasil = []
        angka = "Angka "
        genap = " adalah genap"
        ganjil = " adalah ganjil"
        for i in range(a,b+1):
            if (i%2)==0:
                hasil.append(angka + str(i) + genap)
            else:
                hasil.append(angka + str(i) + ganjil)
        return render(request, "hasil.html", {"result": hasil})
    else:
        hasil = "Angka tidak valid"
        return render(request, "hasil.html", {"result": hasil})
