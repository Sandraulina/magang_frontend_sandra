CREATE DATABASE RentalBuku

USE RentalBuku

CREATE TABLE [User](
	user_id varchar(3) primary key,
	nama varchar(255) not null,
	email varchar(50) not null,
	no_telpon varchar(15) not null,
	alamat varchar(255) not null,
	password varchar(50) not null,
	roles_id varchar(3)
);

CREATE TABLE [Roles](
	id varchar(3) primary key,
	role varchar(100) not null
);

CREATE TABLE [Books](
	id varchar(3) primary key,
	genre_id varchar(3) not null,
	judul_buku varchar(255) not null,
	deskripsi varchar(255) not null,
	gambar varchar(100) not null,
	harga varchar(100) not null,
	pemilik_id varchar(3) not null,
	peminjam_id varchar(3) not null,
	status varchar(15) not null,
	publish varchar(15) not null
);

CREATE TABLE [Genre](
	genre_id varchar(3) primary key,
	genre varchar(100) not null
);