from django.db import models

# Create your models here.
class roles(models.Model):
    role = models.CharField(max_length=100)

def __str__(self):
    return self.role

class users(models.Model):
    nama = models.CharField(max_length=255)
    email = models.CharField(max_length=50)
    no_telpon = models.CharField(max_length=15)
    alamat = models.CharField(max_length=255)
    password = models.CharField(max_length=50)
    roles_id = models.ForeignKey(roles, on_delete=models.CASCADE, null=True)

def __str__(self):
    return self.nama

class genre(models.Model):
    genre = models.CharField(max_length=100)

def __str__(self):
    return self.genre
    
class books(models.Model):
    judul_buku = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=255)
    gambar = models.CharField(max_length=100)
    harga = models.CharField(max_length=100)
    status = models.CharField(max_length=15)
    publish = models.CharField(max_length=15)
    pemilik_id = models.ForeignKey(users, on_delete=models.CASCADE, null=True)
    peminjam_id = models.ForeignKey(users, on_delete=models.CASCADE, null=True)
    genre_id = models.ForeignKey(genre, on_delete=models.CASCADE, null=True)

def __str__(self):
    return self.judul_buku


