from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, "input.html")

def cek(request):
    vokal = "AaIiUuEeOo"
    text = request.POST['text']
    lowtext =  text.lower()
    hasil = {each for each in lowtext if each in vokal}
    cekvokal = text + " = " + str(len(hasil)) + " yaitu " + str(hasil)
    return render(request, "output.html", {"output": cekvokal})
